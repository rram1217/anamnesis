function validar() {
	var documento_usuario,contrasena_usuario;
	documento_usuario = document.getElementById("documento_usuario").value;
	contrasena_usuario = document.getElementById("contrasena_usuario").value;

	if (documento_usuario === "" || contrasena_usuario === "") {
		alert("Todos los campos son obligatorios");
		return false;
	}
	else if (documento_usuario.length>10) {
		alert("El Documento tiene que ser 10 caracteres");
		return false;
	}
	else if (contrasena_usuario.length>16) {
		alert("La Contraseña tiene que ser de 16 caracteres");
		return false;
	}
}
