<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../modelo/css/bootstrap.min.css" rel="stylesheet">
		<link href="../modelo/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../js/jquery-3.1.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>	
		<?php require '../modelo/favicon.php'; ?>
	</head>
	<body>
	<!--
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">NUEVO REGISTRO</h3>
			</div> -->
		<!-- Formado con el fondo -->
		<div class="container">
			<div id="signupbox" style="margin-top:60px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
				<div class="panel-title"><h3 style="text-align:center"> REGISTRAR FORMACION</h3> 
				</div>
				</div>
				<div style="float:right; font-size:50%; position:relative; top:-10px"><a id="singinlink" >...</a></div>
<?php 
				require '../controlador/conexion.php';
				$sql = $mysqli -> query("SELECT * FROM formacion");
				$bandera=0;	
				while($row = mysqli_fetch_assoc($sql))
				{
					$esta = $row['estado'];
					if ($esta==1) {
						$bandera=$bandera+1;
						# code...
					}
				}
	if($bandera>=2)
	{
		echo "<h2><center>Se encuentra una formacion activa</center></h2>" ;
	?>
	<html>
	<br>
	<div class="form-group">
			<div align="center" ">
				<a href="menu.php" class="btn btn-default">Regresar</a>
			</div>
		</div>
	<?php	
	}


	if($bandera<2)
	{
?>
<body>

<form class="form-horizontal" method="POST" action="../controlador/formacion1.php" enctype="multipart/form-data" autocomplete="off" onsubmit="return validar();">
	<div class="form-group">
		<label class="col-sm-2 control-label">N° de Ficha</label>
			<div class="col-sm-10">
				<input  class="form-control" type="number" name="codigoficha" value="" required="">
			</div>
		</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Nombre de la Formacion</label>
			<div class="col-sm-10">
				<input  class="form-control" type="text" name="nombreformacion" value="" required="">
			</div>
		</div>
	<?php 
	$fecha_Actual=date("Y-m-d");
	$fecha_fin=date("Y-m-d", strtotime("$fecha_Actual + 1 day"));
	?>
<div class="form-group">
		<label class="col-sm-2 control-label">Fecha de Inicio </label>
			<div class="col-sm-10">
				<?php echo "<input class='form-control' type='date' value='$fecha_Actual' required='' min=".$fecha_Actual." name='fechainiciar'>";?>
			</div>
		</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">Fecha Fin</label>
			<div class="col-sm-10">
				<?php echo "<input class='form-control' type='date' required='' min=".$fecha_fin." name='fechafin'>";?>
			</div>
		</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nivel</label>
					<div class="col-sm-10">
						<label class="radio-inline">
							<input type="radio" name="nivel" value="tecnico" checked> TECNICO
						</label>
						<label class="radio-inline">
							<input type="radio" name="nivel" value="tecnologo"> TECNOLOGO
						</label>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Jornada</label>
					<div class="col-sm-10">
						<label class="radio-inline">
							<input type="radio" name="jornada" value="mañana" checked> MAÑANA
						</label>
						<label class="radio-inline">
							<input type="radio" name="jornada" value="tarde"> TARDE
						</label>
						<label class="radio-inline">
							<input type="radio" name="jornada" value="noche"> NOCHE
						</label>
					</div>
				</div>
			<div class="form-group">
		<label class="col-sm-2 control-label">Hora de Ingreso</label>
			<div class="col-sm-10">
				<?php echo "<input class='form-control' type='time' required='' name='horainiciar'>";?>
			</div>
		</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">Hora de Salida</label>
			<div class="col-sm-10">
				<?php echo "<input class='form-control' type='time' required='' name='horafinalizar'>";?>
			</div>
		</div></div>
		<div class="form-group" align="center">
				<button type="submit" class="btn btn-primary">Guardar</button>
		</div>	
		<br><br><br>							
</form>
</body>
</html>
<?php } ?>