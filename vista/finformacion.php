<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../modelo/css/bootstrap.min.css" rel="stylesheet">
		<link href="../modelo/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../modelo/js/jquery-3.1.1.min.js"></script>
		<script src="../modelo/js/bootstrap.min.js"></script>	
		<?php require '../modelo/favicon.php'; ?>
	</head>
	<body>
		<br><br><br><br>
		<div class="container">
			<div id="signupbox" style="margin-top:60px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
				<div class="panel-title"><h3 style="text-align:center">Eliminar Ficha</h3> 
				</div>
				</div>
				<div style="float:right; font-size:50%; position:relative; top:-10px"><a id="singinlink" href="index.php">...</a></div>
			<form class="form-horizontal" name="finformacion.php" method="POST" action="../controlador/finformacion.php">			
				<div class="form-group">
					<label class="col-sm-2 control-label">Ficha</label>
						<div class="col-sm-10">
					<input  class="form-control" autofocus="" placeholder="Codigo de Ficha" type="number" name="codificha" step="any" value="" required="">
				</div>
			</div>
		<div class="form-group" align="center">
			<button type="submit" class="btn btn-primary" name="finalizar">Guardar</button>
		</div>
	</form>
	</div>
	</body>
</html>