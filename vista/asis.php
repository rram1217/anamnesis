<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script src="../modelo/js/jquery-3.1.1.min.js"></script>
	<script src="../modelo/js/bootstrap.min.js"></script>
	<script src="../modelo/js/jquery.dataTables.min.js"></script>
	<?php require '../modelo/favicon.php'; ?>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h1>LISTA DE PACIENTES CON HISTORIALES CLINICOS</h1>
		</div>
		<div class="row">
			<a href="index.php?page=registrar_aprendiz1" class="btn btn-primary">Agregar Paciente</a>
			<!-- <a href="archivo.php" target="_blank" class="btn btn-danger">Imprimir Historial</a> -->
		<br>
		</div>
		<br>
	<div class="row table-responsive">
		<table class="display" id="mitabla">
			<thead>
				<tr>
				<th>ID</th>
				<th>NOMBRE</th>
				<th>APELLIDO</th>
				<th>DIA</th>
				<th>SEXO</th>
				<th>TELEFONO</th>
				<th>HISTORIAL</th>
				<th>IMPRIMIR</th>
				</tr>
			</thead>
</body>
</html>
<?php  
	require '../controlador/conexion.php';	
		$resultado = $mysqli->query($sql = "SELECT * FROM paciente ");
	?>
	<html lang="es">
			<tbody>
			<?php while($row = mysqli_fetch_assoc($resultado))
			{ 
			?>
			<tr>
			<td><?php echo $row['tip_doc']." ".$row['doc_paciente']; ?></td>
			<td><?php echo $row['nombre']; ?></td>
			<td><?php echo $row['apellidos']; ?></td>
		<?php  
		    $date=date("Y-m-d");
			$resultado1 = $mysqli->query($sql = "SELECT * FROM historia WHERE paciente_id=".$row['paciente_id']." ORDER BY fecha_recepcion DESC");
			$row1 = mysqli_fetch_assoc($resultado1);
			?>
			<td><?php echo $row1['fecha_recepcion']; ?></td>
			<td><?php echo $row['sexo']; ?></td>
			<td><?php echo $row['telefono']; ?></td>
			<td><a href="index.php?page=asistencia&id=<?php echo $row['doc_paciente']; ?>"><span class="glyphicon glyphicon-pencil"></span></span></a></td>
			<td><a href="archivo.php?id=<?php echo $row['paciente_id']; ?>" target="_blank" ><span class="glyphicon glyphicon-pencil"></span></span></a></td>
			</tr>
	<?php  
		}
	?>
</tbody>
</table>
</div>
</div>
</body>
</html>