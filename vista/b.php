<?php
	require '../controlador/conexion.php';
	$id_actividad = $_GET['id_ac'];
	$sql = "SELECT * FROM actividad WHERE id = '$id_actividad'";
	$resultado = $mysqli->query($sql);
	$rows = mysqli_fetch_assoc($resultado);	
	?> 
<html lang="es">
<head>
	 <meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script src="../modelo/js/jquery-3.1.1.min.js"></script>
	<script src="../modelo/js/bootstrap.min.js"></script>
	<script src="../modelo/js/jquery.dataTables.min.js"></script>
	<?php require '../modelo/favicon.php'; ?>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
		<h1>NOTAS DE LA ACTIVIDAD <?php echo strtoupper($rows['actividad']); ?> </h1>
		</div>
<div class="row">
	<form>
	<a href="agregar_actividad.php" class="btn btn-primary">Agregar Actividad</a>
	</form>
		<br> 
		</div>
		<br>
		<div class="row table-responsive">
		<table class="display" id="mitabla" border="0">
			<thead>
				<tr>
					<th></th><th></th><th width="250px" colspan="10" bgcolor="#8CC8FE"><center>NOTAS</center></th><th></th><th></th>
				</tr>
				<tr>
				<th>DOCUMENTO</th>
				<th>NOMBRES</th>
				<th bgcolor="#8CC8FE">1</th>
				<th bgcolor="#8CC8FE">2</th>
				<th bgcolor="#8CC8FE">3</th>
				<th bgcolor="#8CC8FE">4</th>
				<th bgcolor="#8CC8FE">5</th>
				<th bgcolor="#8CC8FE">6</th>
				<th bgcolor="#8CC8FE">7</th>
				<th bgcolor="#8CC8FE">8</th>
				<th bgcolor="#8CC8FE">9</th>
				<th bgcolor="#8CC8FE">10</th>
				<th><b>DEFINITIVA</b></th>
				<th>EDITAR</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			$resultado = $mysqli->query($sql = "SELECT * FROM aprendiz ");
			while($row_a = mysqli_fetch_assoc($resultado))
				{
					$documento_apr=$row_a['documento'];
			?>
			<tr>
			<td><?php echo $row_a['tipo_documento']."-".$row_a['documento']; ?></td>
			<td><?php echo $row_a['nombres_a']." ".$row_a['apellidos_a']; ?></td>
			<?php  
			$result = $mysqli->query("SELECT * FROM tblnotas WHERE documento='$documento_apr' AND id_act='$id_actividad'");
			$row_n = mysqli_fetch_assoc($result);
			?>
			<td align="center"><?php if (isset($row_n['nota1'])) {
				echo round($row_n['nota1'],2);
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota2'])) {
				echo round($row_n['nota2'],2);
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota3'])) {
				echo round($row_n['nota3'],2);
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota4'])) {
				echo round($row_n['nota4'],2);
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota5'])) {
				echo round($row_n['nota5'],2); 
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota6'])) {
				echo round($row_n['nota6'],2); 
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota7'])) {
				echo round($row_n['nota7'],2); 
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota8'])) {
				echo round($row_n['nota8'],2); 
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota9'])) {
				echo round($row_n['nota9'],2); 
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><?php if (isset($row_n['nota10'])) {
				echo round($row_n['nota10'],2);
			} else {
				echo "-";
			}
			?></td>
			<td align="center"><b><?php if ($row_n['definitiva']) {
				echo round($row_n['definitiva'],2);
			} else {
				echo "-";
			}
			?></b></td>
			<td align="center"><a href="registrar_nota.php?id_acti=<?php echo $row_n['id_act']; ?>&id_apr=<?php echo $row_a['documento']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td></tr>
			<?php  
				}
			?>
			</tbody>
		</table> 
		</div>
		<br><div class="form-group" align="center">
				<a href="a.php" 	class="btn btn-default">Regresar</a>
				<a href="menu.php" 	class="btn btn-primary">Menu Principal</a>
		</div>
		<div class="form-group">
		<div align="center">
        <center> <b class="copyright"><a > Sistema de Notas </a> &copy; <?php echo date("Y")?> Grupo ADSI   </b></center>
            </div>
            <br>
            <br>
        </div>
</body>
</html>								