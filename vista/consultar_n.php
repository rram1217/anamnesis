<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<head>
	 <meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script src="../modelo/js/jquery-3.1.1.min.js"></script>
	<script src="../modelo/js/bootstrap.min.js"></script>
	<script src="../modelo/js/jquery.dataTables.min.js"></script>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h1>APRENDIZ </h1>
		</div>
		<div class="row">
		<br>
		</div>
		<br>
<div class="row table-responsive">
		<table class="display" id="mitabla">
			<thead>
				<tr>
				<th>#</th>
				<th>DOCUMENTO</th>
				<th>CODIGO ACTIVIDAD</th>
				<th>NOTA1</th>
				<th>NOTA2</th>
				<th>NOTA3</th>
				<th>NOTA4</th>
				<th>NOTA5</th>
				<th>NOTA6</th>
				<th>NOTA7</th>
				<th>NOTA8</th>
				<th>NOTA9</th>
				<th>NOTA10</th>
				<th>DEFINITIVA</th>
				<th></th>
				</tr>
			</thead>

</body>
</html>
<?php  
require '../controlador/conexion.php';
	$sql = "SELECT * FROM tblnotas";
	$resultado = $mysqli->query($sql);
	?> 
<html lang="es">
			<tbody>	
			<tr>
			<?php while($row = mysqli_fetch_assoc($resultado)){?>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['id_a']; ?></td>
			<td><?php echo $row['id_act']; ?></td>
			<td><?php echo round($row['nota1'],2); ?></td>
			<td><?php echo round($row['nota2'],2); ?></td>
			<td><?php echo round($row['nota3'],2); ?></td>
			<td><?php echo round($row['nota4'],2); ?></td>
		    <td><?php echo round($row['nota5'],2); ?></td>
		    <td><?php echo round($row['nota6'],2); ?></td>
		    <td><?php echo round($row['nota7'],2); ?></td>
		    <td><?php echo round($row['nota8'],2); ?></td>
		    <td><?php echo round($row['nota9'],2); ?></td>
		    <td><?php echo round($row['nota10'],2); ?></td>
		    <td><?php echo round($row['definitiva'],2); ?></td>
	</tr>
	<?php
}	
?>
</tbody>
</table>
</div>
</div>
<br><br>
        <div class="form-group">
					<div  align="center">
						<a  href="../menu.php" class="btn btn-default">Regresar</a>
					</div>
				</div>
				<br>
<br>
		<div class="form-group">
		<div align="center">
        <center> <b class="copyright"><a > Sistema de Notas </a> &copy; <?php echo date("Y")?> Grupo ADSI   </b></center>
            </div>
            <br>
            <br>
        </div>
</body>
</html>