<?php
/*
	require '../controlador/conexion.php';
		$res = $mysqli->query("SELECT * FROM formacion WHERE estado=1")or die("<script>alert('No se encuentran Aprendices registrados a la formacion activa');window.location.href='../vista/menu.php';</script>");
		$row_formacion = mysqli_fetch_assoc($res);
	$resultado = $mysqli->query("SELECT * FROM actividad WHERE codigoficha=".$row_formacion['codigoficha']." ")or die("<script>alert('No se encuentran Aprendices registrados a la formacion activa');window.location.href='../vista/menu.php';</script>");	
	*/
	?> 
<html lang="es">
<head>
	 <meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script src="../modelo/js/jquery-3.1.1.min.js"></script>
	<script src="../modelo/js/bootstrap.min.js"></script>
	<script src="../modelo/js/jquery.dataTables.min.js"></script>
	<?php require '../modelo/favicon.php'; ?>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
		<h1>ACTIVIDAD </h1>
		</div>
		<div class="row">
			<a href="agregar_actividad.php" class="btn btn-primary">Agregar Actividad</a>
		<br> 
		</div>
		<br>
		<div class="row table-responsive">
		<table class="display" id="mitabla" border="0">
			<thead>
				<tr>
				<th align="center" >ID</th>
				<th align="center">ACTIVIDADES</th>
				<th align="center" width="70px">EDITAR </th>
				<th align="center" width="70px">AGREGAR NOTA</th>
				<th align="center" width="70px">ELIMINAR</th>
				</tr>
			</thead>
			<tbody>
			<?php while($row = mysqli_fetch_assoc($resultado))
			{	?>
			<tr>
			<td ><?php echo $row['id']; ?></td>
			<td ><?php echo $row['actividad']; ?></td>
			<td align="center"><a href="a_actu.php?id=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
			<td align="center" ><a href="b.php?id_ac=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
			<td align="center" ><a href="../controlador/a_eliminar.php?id_el=<?php echo $row['id']; ?>"><span class="glyphicon glyphicon-remove"></span></a></td>
			<?php }?>
			</tbody>
		</table> 
		</div>
</body>
</html>								