<?php
	include 'plantilla.php';
	require '../controlador/conexion.php';
	$resultado = $mysqli->query("SELECT * FROM paciente");
	$row = mysqli_fetch_assoc($resultado);

	$pdf = new PDF('P','mm','Letter'); 
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetLeftMargin(22);
	$pdf->SetFillColor(232,232,232);
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(195,6, utf8_decode('INFORME DE EVALUACIÓN'),0,1,'C');
	$pdf->Ln(6);
	$pdf->SetFont('Arial','B',14);
	$pdf->Cell(195,6, utf8_decode('DATOS PERSONALES'),0,1,'L');
	$pdf->Ln(6);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(90,6,'NOMBRE: '.utf8_decode($row['nombre']." ".$row['apellidos']),1,0,'L');
	$pdf->Cell(35,6,'SEXO: '.utf8_decode($row['sexo']),1,0,'L');
	$pdf->Cell(45,6,''.utf8_decode($row['tip_doc']." ".$row['doc_paciente']),1,1,'L');
	
	$pdf->Cell(90,6,'FECHA DE NACIMIENTO: ',1,0,'L');
	$pdf->Cell(35,6,'EDAD: ',1,0,'L');
	$pdf->Cell(45,6,'ESCOLARIDAD: ',1,1,'L');
	
	$pdf->Cell(90,6,'LUGAR DE NACIMIENTO: ',1,0,'L');
	$pdf->Cell(35,6,'TELEFONO: ',1,0,'L');
	$pdf->Cell(45,6,'DIRECCION: ',1,1,'L');
	
	$pdf->Cell(90,6,'OCUPACION: ',1,0,'L');
	$pdf->Cell(35,6,'EPS: ',1,0,'L');
	$pdf->Cell(45,6,'T. DE VINCULACION: ',1,1,'L');
	
	$pdf->Cell(90,6,'LATERALIDAD: ',1,0,'L');
	$pdf->Cell(80,6,'ESTADO CIVIL: ',1,1,'L');
	$pdf->Ln(6);
	
	$pdf->Cell(85,6,'FECHA DE RECEPCION DEL CASO: ',1,0,'L');
	$pdf->Cell(85,6,'FECHA CIERRE CASO: ',1,1,'L');
	
	$pdf->Cell(85,6,'REMITIDO POR: ',1,0,'L');
	$pdf->Cell(85,6,'RESPONSABLE: ',1,1,'L');
	
	$pdf->Ln(6);
	
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(85,6,'MOTIVO DE CONSULTA',0,1,'L');
	$pdf->Ln(6);
	
	$pdf->Cell(85,6,'DATOS DE HISTORIA CLINICA',0,1,'L');
	$pdf->SetFont('Arial','B',11);
	$pdf->Ln(6);
	
	$pdf->Cell(85,6,'Antecedentes',0,1,'L');
	$pdf->SetFont('Arial','',10);
	
	$pdf->Cell(80,6,utf8_decode('Patológicos: '),0,1,'L');
	
	$pdf->Cell(80,6,utf8_decode('Quirúrgicos: '),0,1,'L');
	
	$pdf->Cell(80,6,utf8_decode('Traumáticos: '),0,1,'L');
	
	$pdf->Cell(80,6,utf8_decode('Tóxico-alérgico: '),0,1,'L');
	
	$pdf->Cell(80,6,utf8_decode('Farmacológicos: '),0,1,'L');
	
	$pdf->Cell(80,6,utf8_decode('Familiares: '),0,1,'L');
	
	$pdf->Cell(80,6,utf8_decode('Psiquiátricos: '),0,1,'L');

	$pdf->AddPage();

	
	$pdf->SetFont('Arial','B',14);
	$pdf->Cell(195,6, utf8_decode('RESULTADOS CUANTITATIVOS'),0,1,'L');
	$pdf->Ln(6);
	$pdf->SetFont('Arial','',10);
	
	$pdf->Cell(43,6,'PRUEBA REALIZADA',1,0,'C',1);
	$pdf->Cell(43,6,'PUNTAJES ESPERADOS',1,0,'C',1);
	$pdf->Cell(43,6,'PACIENTE',1,0,'C',1);
	$pdf->Cell(43,6,'CLASIFICACION',1,1,'C',1);

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

$pdf->AddPage();

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,0,'L');
	$pdf->Cell(43,6,'',1,1,'L');

$pdf->AddPage();
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(195,6, utf8_decode('INFORME DE EVALUACIÓN'),0,1,'C');
	$pdf->Ln(6);
	
	$pdf->SetFont('Arial','B',14);
	$pdf->Cell(195,6, utf8_decode('DATOS PERSONALES'),0,1,'L');
	$pdf->Ln(6);
	$pdf->SetFont('Arial','',10);
	
	$pdf->Cell(90,6,'NOMBRE: ',1,0,'L');
	$pdf->Line(60, 180, 150, 180);

	$pdf->Output();
?>