<html lang="es">
	<head>		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../modelo/css/bootstrap.min.css" rel="stylesheet">
		<link href="../modelo/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../modelo/js/jquery-3.1.1.min.js"></script>
		<script src="../modelo/js/bootstrap.min.js"></script>	
			<script type="text/javascript">
			$(document).ready(function(){
				$('.delete').click(function(){
					var parent = $ (this).parent().attr('id');
					var service = $ (this).parent().attr('data');
					var dataString = 'id='+service;
					$.ajax({
						type: "POST",
						url: "del_file.php",
						data: dataString,
						success: function(){
							location.reload();
						}
					});
				});
			});
		</script>		
	</head>
	<div class="container">
			<div id="signupbox" style="margin-top:60px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
				<div class="panel-title"><h3 style="text-align:center"> ACTUALIZAR NOTAS</h3> 
				</div>
				</div>
				<div style="float:right; font-size:50%; position:relative; top:-10px"><a id="singinlink" href="index.php">...</a></div>		
			<form class="form-horizontal" method="POST" action="../controlador/actu_notas2.php" enctype="multipart/form-data" autocomplete="off">
				<div class="form-group">
					<label class="col-sm-2 control-label">ID</label>
					<div class="col-sm-10">
						<input type="number" id="id" class="form-control"  name="id" placeholder="Id" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">ID APRENDIZ</label>
					<div class="col-sm-10">
					<input type="number" id="id" class="form-control"  name="idapren" placeholder="Ide del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"> ID ACTIVIDAD </label>
					<div class="col-sm-10">
						<input type="number" id="id" class="form-control"  name="idactivi" placeholder="Ide de la actividad" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">NOMBRE</label>
					<div class="col-sm-10">
						<input type="text" class="form-control"  name="nomb" placeholder="Nombre del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">APELLIDO</label>
					<div class="col-sm-10">
						<input type="text" class="form-control"  name="apellid" placeholder="Apellidos del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 1</label>
					<div class="col-sm-10">
						<input type="varchar" class="form-control"  name="not1" placeholder="Nota del aprendiz" value=""  required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 2</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not2" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
                <div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 3</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not3" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 4</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not4" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 5</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not5" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 6</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not6" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 7</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not7" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
                <div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 8</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not8" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 9</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not9" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">NOTA 10</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="not10" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">DEFINITIVA</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="def" placeholder="Nota del aprendiz" value="" required>
					</div>
				</div>
			<div class="form-group" align="center">
						<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	<br>
	<div class="form-group">
			<div align="center" ">
				<a href="menu.php" class="btn btn-default">Regresar</a>
			</div>
		</div>
	<br><br>
		<div class="form-group">
		<div align="center">
        <center> <b class="copyright"><a > Sistema de Notas </a> &copy; <?php echo date("Y")?> Grupo ADSI   </b></center>
            </div>
            <br>
            <br>
        </div>