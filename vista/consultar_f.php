<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <meta name = "viewport" content = "width=device-width, initial-scale=1">
	 <meta charset="utf-8" lang="es">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script src="../modelo/js/jquery-3.1.1.min.js"></script>
	<script src="../modelo/js/bootstrap.min.js"></script>
	<script src="../modelo/js/jquery.dataTables.min.js"></script>
	<?php require '../modelo/favicon.php'; ?>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h1>FORMACION </h1>
		</div>
		<div class="row">
			<a href="formacion.php" class="btn btn-primary">Agregar Formacion</a>
		</div>
		<br>
<div class="row table-responsive">
		<table class="display" id="mitabla">
			<thead>
				<tr>
				<th>CODIGO FICHA</th>
				<th>NOMBRE FORMACION</th>
				<th>FECHA INICIAR</th>
				<th>FECHA FIN</th>
				<th>NIVEL</th>
				<th>JORNADA</th>
				<th>HORA ENTRADA</th>
				<th>HORA SALIDA</th>
				<th>ESTADO</th>
				<th></th>
				</tr>
			</thead>
</body>
</html>
<?php  
require '../controlador/conexion.php';
	$sql = "SELECT * FROM formacion ";
	$resultado = $mysqli->query($sql);
	?> 
<html lang="es">		
			<tbody>
			<tr>
			<?php while($row = mysqli_fetch_assoc($resultado))
			{
				$esta=$row['estado'];
	if($esta==0)
	{
		$a="INACTIVO";
	}
	if($esta==1)
	{
		$a="ACTIVO";
	}
				?>
			<td><?php echo $row['codigoficha']; ?></td>
			<td><?php echo $row['nombreformacion']; ?></td>
			<td><?php echo $row['fechainiciar']; ?></td>
			<td><?php echo $row['fechafin']; ?></td>
			<td><?php echo $row['nivel']; ?></td>
			<td><?php echo $row['jornada']; ?></td>
			<td><?php echo $row['hora_entrada']; ?></td>
			<td><?php echo $row['hora_salida']; ?></td>
			<td><?php echo $a; ?></td>
			<td><a href="actu_formacion.php?id=<?php echo $row['codigoficha']; ?>"><span class="glyphicon glyphicon-pencil"></span></span></a></td>
	</tr>
	<?php
}	
?>
</tbody>
</table>
</div>
</div>
</body>
</html>