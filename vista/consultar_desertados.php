<?php 
	require '../controlador/conexion.php';	
		$resultado = $mysqli->query($sql = "SELECT * FROM formacion WHERE estado=1");
		$row_forma = mysqli_fetch_assoc($resultado);
 ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
    <meta charset="utf-8" lang="es">
	 <meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script src="../modelo/js/jquery-3.1.1.min.js"></script>
	<script src="../modelo/js/bootstrap.min.js"></script>
	<script src="../modelo/js/jquery.dataTables.min.js"></script>
	<?php require '../modelo/favicon.php'; ?>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h1>APRENDICES REGISTRADOS A LA FORMACION </h1>
		</div>
		<div class="row">
			<a href="registrar_aprendiz1.php" class="btn btn-primary">Agregar Aprendiz</a>
			<a  href='menu.php' class='btn btn-danger'>Menu</a>
		<br>
		</div>
		<br>
	<div class="row table-responsive">
		<table class="display" id="mitabla">
			<thead>
				<tr>
					<th>DOCUMENTO</th>
					<th>NOMBRES</th>
					<th>DIRECCION</th>
					<th>TELEFONO</th>
					<th>SEXO</th>
					<th>CORREO</th>
					<th>N° DE FICHA</th>
					<th>NOMBRE FAMILIAR</th>
					<th>TELEFONO FAMILIAR</th>
					<th>EDITAR</th>
				</tr>
			</thead>
</body>
</html>
<?php  
	require '../controlador/conexion.php';	
		$resultado = $mysqli->query($sql = "SELECT * FROM desertados WHERE codigoficha=".$row_forma['codigoficha']."")or die("<script>alert('No se encuentran Aprendices Desertados a la formacion activa');window.location.href='../vista/menu.php';</script>");
	?> 

<html lang="es">
		<tbody>
			<?php while($row = mysqli_fetch_assoc($resultado)){ ?>
			<tr>
			<td><?php echo $row['tipo_documento']."-".$row['documento']; ?></td>
			<td><?php echo $row['nombres_a']." ".$row['apellidos_a']; ?></td>
			<td><?php echo $row['direccion_a']; ?></td>
			<td><?php echo $row['telefono_a']; ?></td>
			<td><?php echo $row['sexo_a']; ?></td>
			<td><?php echo $row['correo_a']; ?></td>
			<td><?php echo $row['codigoficha']; ?></td>
			<td><?php echo $row['nombre_familiar']; ?></td>
			<td><?php echo $row['telefono_familiar']; ?></td>
			<td><a href="actu_aprendiz.php?id=<?php echo $row['documento']; ?>"><span class="glyphicon glyphicon-pencil"></span></span></a></td>
		</tr>
	<?php
		}	
	?>
</tbody>
</table>
</div>
</div>
</body>
</html>