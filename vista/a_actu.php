<?php
	require '../controlador/conexion.php';
	$id = $_GET['id'];
	$sql = "SELECT * FROM actividad WHERE id = '$id'";
	$resultado = $mysqli->query($sql);
	$row = mysqli_fetch_assoc($resultado);
?>
<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../modelo/css/bootstrap.min.css" rel="stylesheet">
		<link href="../modelo/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../modelo/js/jquery-3.1.1.min.js"></script>
		<script src="../modelo/js/bootstrap.min.js"></script>	
		<?php require '../modelo/favicon.php'; ?>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.delete').click(function(){
					var parent = $ (this).parent().attr('id');
					var service = $ (this).parent().attr('data');
					var dataString = 'id='+service;
					$.ajax({
						type: "POST",
						url: "del_file.php",
						data: dataString,
						success: function(){
							location.reload();
						}
					});
				});
			});
		</script>
	</head>
<div class="container">
			<div id="signupbox" style="margin-top:60px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
				<div class="panel-title"><h3 style="text-align:center"> ACTUALIZAR ACTIVIDAD</h3> 
				</div>
				</div>
				<div style="float:right; font-size:50%; position:relative; top:-10px"><a id="singinlink" href="index.php">...</a></div>
			<form class="form-horizontal" method="POST" action="../controlador/a_actu_1.php" enctype="multipart/form-data" autocomplete="off">
				<input type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>" />
				<div class="form-group">
					<label class="col-sm-2 control-label">Actividad</label>
					<div class="col-sm-10">
						<input type="text" class="form-control"  name="actividad"  placeholder="Nombre del aprendiz" value="<?php echo $row['actividad']; ?>" autofocus="" required>
					</div>
				</div>
				<div class="form-group" align="center">
						<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
		<br>
		<br>
	<div class="form-group">
			<div align="center" ">
				<a href="a.php" class="btn btn-default">Regresar</a>
			</div>
		</div>
	<br><br>
		<div class="form-group">
		<div align="center">
        <center> <b class="copyright"><a > Sistema de Notas </a> &copy; <?php echo date("Y")?> Grupo ADSI   </b></center>
            </div>
            <br>
            <br>
        </div>
	</body>
	</html>