<?php
	require '../controlador/conexion.php';
	$usuario = $_GET['id_u'];
	$id_actividad = $_GET['id_acti'];
	$id_aprendiz = $_GET['id_apr'];
	$resultado = $mysqli->query("SELECT * FROM tblnotas WHERE id_act = '$id_actividad' AND documento='$id_aprendiz'");
	$row = mysqli_fetch_assoc($resultado);
	$result_apr = $mysqli->query("SELECT * FROM aprendiz WHERE documento = '$id_aprendiz'");
	$row_apr = mysqli_fetch_assoc($result_apr);
	$result_acti = $mysqli->query("SELECT * FROM actividad WHERE id = '$id_actividad'");
	$row_acti = mysqli_fetch_assoc($result_acti);
?>
<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../modelo/css/bootstrap.min.css" rel="stylesheet">
		<link href="../modelo/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../js/jquery-3.1.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>	
		<?php require '../modelo/favicon.php'; ?>
	</head>
	<body>
		<div class="container">
			<div id="signupbox" style="margin-top:60px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
				<div class="panel-title"><h3 style="text-align:center"> REGISTRAR HISTORIA CLINICA</h3> 
				</div>
				</div>
				<div style="float:right; font-size:50%; position:relative; top:-10px"><a id="singinlink" >...</a></div>
<body>
<form class="form-horizontal" method="POST" action="../controlador/registrar_nota2.php?id_acti=<?php echo $row_acti['id'];?>&id_apr=<?php echo $row_apr['documento']; ?>" enctype="multipart/form-data" autocomplete="off" onsubmit="return validar();">
	<div class="form-group">
		<label class="col-sm-3 control-label">PACIENTE</label>
			<div class="col-sm-8">
				<input  class='form-control' type='text' name='idnot' value="<?php echo $row_apr['nombres_a']." ".$row_apr['apellidos_a']; ?>" required="" readonly='readonly'>
			</div>
		</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 1</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not1" value=<?php echo $row['nota1']; ?> >
			</div>
		</div>
			<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 2</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not2" value=<?php echo $row['nota2']; ?> >
			</div>
		</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 3</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not3" value="<?php echo $row['nota3']; ?>"  >
			</div>
		</div>
			<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 4</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not4" value="<?php echo $row['nota4']; ?>"  >
			</div>
		</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 5</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not5" value="<?php echo $row['nota5']; ?>" >
			</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 6</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not6" value="<?php echo $row['nota6']; ?>" >
			</div>
		</div>
			<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 7</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not7" value="<?php echo $row['nota7']; ?>" >
			</div>
		</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 8</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not8" value="<?php echo $row['nota8']; ?>" >
			</div>
		</div>
			<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 9</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not9" value="<?php echo $row['nota9']; ?>" >
			</div>
		</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">RESULTADO 10</label>
			<div class="col-sm-8">
				<input  class="form-control" step="0.001" type="number" name="not10" value="<?php echo $row['nota10']; ?>" >
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<a href='b.php?id_ac=<?php echo $id_actividad ?>&&id_u=<?php echo $usuario; ?>' class="btn btn-default">Regresar</a>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
		</div>								
</form>
</body>
</html>