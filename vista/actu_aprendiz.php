   <?php
        require '../controlador/conexion.php';
        $id = $_GET['id'];
        $sql = "SELECT * FROM paciente WHERE doc_paciente = '$id'";
        $resultado = $mysqli->query($sql);
        $row = mysqli_fetch_assoc($resultado);
	?>

	<div class="container">
			<div id="signupbox" style="margin-top:60px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
				<div class="panel-title"><h3 style="text-align:center"> ACTUALIZAR APRENDIZ</h3> 
				</div>
				</div>
				<div style="float:right; font-size:50%; position:relative; top:-10px"><a id="singinlink" href="index.php">...</a></div>
			<form class="form-horizontal" method="POST" name="actu_aprendiz.php" action="../controlador/actu_aprendiz2.php" enctype="multipart/form-data" autocomplete="off">
			    <div class="form-group">
					<label class="col-sm-2 control-label">Tipo Documento</label>
                <div class="col-sm-10">
						<label class="radio-inline">
							<input type="radio" name="tipodocument" value="ti"> T.I.
						</label>
						
						<label class="radio-inline">
							<input type="radio" name="tipodocument" value="cc" checked> C.C.
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Documento</label>
					<div class="col-sm-10">
						<input type="number" id="id" class="form-control" value="<?php echo $row['doc_paciente']; ?>"  name="id" placeholder="Documento" required="" readonly='readonly'>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nombres</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="nombres" value="<?php echo $row['nombre']; ?>" placeholder="Nombre del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Apellidos</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="apellid" value="<?php echo $row['apellidos']; ?>" placeholder="Apellidos del aprendiz" value="" required>
					</div>
				</div>
				
				<div class="form-group">
					<label  class="col-sm-2 control-label">Direccion</label>
					<div class="col-sm-10">
						<input type="varchar" class="form-control" name="direcci" value="<?php echo $row['direccion']; ?>" placeholder="Direccion del aprendiz" value=""  required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Telefono</label>
					<div class="col-sm-10">
						<input type="varchar" class="form-control" name="telefon" value="<?php echo $row['telefono']; ?>" placeholder="Telefono del aprendiz" value="" required>
					</div>
				</div>
                 <div class="form-group">
					<label class="col-sm-2 control-label">Correo</label>
					<div class="col-sm-10">
						<input type="varchar" id="id" class="form-control" name="correo" value="<?php echo $row['correo_a']; ?>" placeholder="Correo del aprendiz" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Sexo</label>
					
					<div class="col-sm-10">
						<label class="radio-inline">
							<input type="radio" name="sexo_ap" value="femenino" checked> FEMENINO
						</label>
						
						<label class="radio-inline">
							<input type="radio" name="sexo_ap" value="masculino"> MASCULINO
						</label>

					</div>
				</div>
		         <div class="form-group">
					<label class="col-sm-2 control-label">Nombre Familiar</label>
					<div class="col-sm-10">
				<input class="form-control" type="text" name="nombrefa" value="<?php echo $row['nombre_familiar']; ?>" placeholder="Nombre del familiar" value="" required="">
			    </div>
			     </div>
		        <div class="form-group">
					<label class="col-sm-2 control-label">Telefono Familiar</label>
					<div class="col-sm-10">
				<input class="form-control" type="varchar" name="telefonofa" value="<?php echo $row['telefono_familiar']; ?>" placeholder="Telefono del aprendiz" value="" required="">
		        </div>
		        </div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Ficha</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" name="fich_a" value="<?php echo $row['codigoficha']; ?>" placeholder="Codigo de ficha" required="" readonly='readonly'>
					</div>
				</div>
				<div class="form-group" align="center">
						<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>