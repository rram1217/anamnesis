
  <?php
		
		session_start();
		if ($_SESSION["autenticado"] != "SI") {
		header("Location: ../index.php");
		exit();
		}


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="../img/icono.ico" />

	<link rel="stylesheet" href="../modelo/css/fontello.css">
    <link rel="stylesheet" href="../modelo/css/menu_estilo2.css">
    <link rel="icon" href="../img/logo.ico">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script src="../modelo/js/jquery-3.1.1.min.js"></script>
	<script src="../modelo/js/bootstrap.min.js"></script>
	<script src="../modelo/js/jquery.dataTables.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="menu.js"></script>
	<!-- <meta http-equiv="refresh" content="60;URL=index.php"> -->

	<title>Anamnesis</title>
		
</head>
<body>
	<?php 
		include('menu.php');
	 ?>
	<div class="container">
		<?php
			if (empty($_GET['page'])==false){
				$pagina = $_GET['page'];
				include($pagina.'.php');
			} else {
				include('asis.php');
			}
		?>
	</div>
</body>
<footer>
	<br><br>
	<center> <b class="copyright"><a > Gestion Anamnesis </a> &copy; <?php echo date("Y")?> Rafael Alvarez - Wilmer Mojica   </b></center>
	<br><br>
</footer>
</html>