<?php
	require '../controlador/conexion.php';
	$id = $_GET['id'];
	$sql = "SELECT * FROM formacion WHERE codigoficha = '$id'";
	$resultado = $mysqli->query($sql);
	$row = mysqli_fetch_assoc($resultado);
?>
<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../modelo/css/bootstrap.min.css" rel="stylesheet">
		<link href="../modelo/css/bootstrap-theme.css" rel="stylesheet">
		<script src="../modelo/js/jquery-3.1.1.min.js"></script>
		<script src="../modelo/js/bootstrap.min.js"></script>	
		<?php require '../modelo/favicon.php'; ?>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.delete').click(function(){
					var parent = $ (this).parent().attr('id');
					var service = $ (this).parent().attr('data');
					var dataString = 'id='+service;
					$.ajax({
						type: "POST",
						url: "del_file.php",
						data: dataString,
						success: function(){
							location.reload();
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">ACTUALIZAR FORMACION</h3>
			</div>
			<form class="form-horizontal" method="POST" action="../controlador/actu_formacion1.php" enctype="multipart/form-data" autocomplete="off">
				<div class="form-group">
					<label class="col-sm-2 control-label">N° de Ficha</label>
					<div class="col-sm-10">
						<input type="number" class="form-control"  name="codficha" value="<?php echo $row['codigoficha']; ?>" required readonly="readonly" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nombre Formacion</label>
					<div class="col-sm-10">
						<input type="text" class="form-control"  name="nomforma" placeholder="Nombre De La Formacion" value="<?php echo $row['nombreformacion']; ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Fecha Iniciar</label>
					<div class="col-sm-10">
						<input type="date" class="form-control"  name="fechaini" placeholder="Fecha De Inicio" value="<?php echo $row['fechainiciar']; ?>"  required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Fecha Fin</label>
					<div class="col-sm-10">
						<input type="date" class="form-control"  name="fechaf" placeholder="Fecha Fin" value="<?php echo $row['fechafin']; ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Nivel</label>
					<div class="col-sm-10">
						<label class="radio-inline">
							<input type="radio" name="niv" value="tecnico" checked> TECNICO
						</label>
						<label class="radio-inline">
							<input type="radio" name="niv" value="tecnologo"> TECNOLOGO
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Jornada</label>
					<div class="col-sm-10">
						<label class="radio-inline">
							<input type="radio" name="jornada" value="mañana" checked> MAÑANA
						</label>
						<label class="radio-inline">
							<input type="radio" name="jornada" value="tarde"> TARDE
						</label>
						<label class="radio-inline">
							<input type="radio" name="jornada" value="noche"> NOCHE
						</label>
					</div>
				</div>
				<div class="form-group">
		<label class="col-sm-2 control-label">Hora de Ingreso</label>
			<div class="col-sm-10">
				<input class='form-control' type='time' value='<?php echo $row['hora_entrada']; ?>' required='' name='horainiciar'>
			</div>
		</div>
		<div class="form-group">
		<label class="col-sm-2 control-label">Hora de Salida</label>
			<div class="col-sm-10">
				<input class='form-control' type='time' value='<?php echo $row['hora_salida']; ?>' required='' name='horafinalizar'>
			</div>
		</div>
<div class="form-group">
					<label  class="col-sm-2 control-label">Estado</label>
					<div class="col-sm-10">
				
				<?php 
				require '../controlador/conexion.php';
	$sql = "SELECT * FROM formacion WHERE codigoficha = '$id'";
	$resultado = $mysqli->query($sql);
	$row = mysqli_fetch_assoc($resultado);
	$esta=$row['estado'];
	if($esta==0)
	{
		$a="INACTIVO";
	}
	if($esta==1)
	{
		$a="ACTIVO";
	}
	echo "<input type='text' class='form-control' name='estad' value='".$a."' required='' readonly='readonly'> ";
				?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="consultar_f.php" class="btn btn-default">Regresar</a>
						<button type="submit" class="btn btn-primary">Guardar</button>
					</div>
				</div>