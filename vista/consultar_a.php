<!DOCTYPE html>
<html>
<head>
	<title></title>
    <meta charset="utf-8" lang="es">
	 <meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.min.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap.css">
	<link rel="stylesheet" type="" href="../modelo/css/bootstrap-theme.css">
	<link rel="stylesheet" type="" href="../modelo/css/jquery.dataTables.min.css">
	<script src="../modelo/js/jquery-3.1.1.min.js"></script>
	<script src="../modelo/js/bootstrap.min.js"></script>
	<script src="../modelo/js/jquery.dataTables.min.js"></script>
	<?php require '../modelo/favicon.php'; ?>
	<script>
		$(document).ready(function(){
			$('#mitabla').DataTable({
				"language":{
					"lengthMenu": "Mostrar_MENU_Registros por pagina",
					"info": "Mostrando pagina _PAGE_ de _PAGES_",
					"infoEmpty": "No hay registros disponibles",
					"infoFiltered": "(filtrada de _MAX_ registros)",
					"loadingRecords": "Cargando...",
					"Processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "No se encontraron Registros",
					"paginate": {
						"next": "Siguente",
						"previous": "Anterior"
					} 
				}
			});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h1>PACIENTES REGISTRADOS </h1>
		</div>
		<div class="row">
			<a href="registrar_paciente1.php" class="btn btn-primary">Agregar paciente</a>
		<br>
		</div>
		<br>
	<div class="row table-responsive">
		<table class="display" id="mitabla">
			<thead>
				<tr>
				<th>DOCUMENTO</th>
				<th>NOMBRES</th>
				<th>DIRECCION</th>
				<th>TELEFONO</th>
				<th>SEXO</th>
				<th>FECHA NACIMIENTO</th>
				<th>LUGAR NACIMIENTO</th>
				<th>ESCOLARIDAD</th>
				<th>LATERALIDAD</th>
				<th>EDITAR</th>
				</tr>
			</thead>				
</body>
</html>
<?php  
	require '../controlador/conexion.php';	
		/*$resultado = $mysqli->query($sql = "SELECT * FROM formacion WHERE estado=1");
		$row_forma = mysqli_fetch_assoc($resultado);*/
		$resultado = $mysqli->query("SELECT * FROM paciente ")or die("<script>alert('No se encuentran Pacientes registrados');window.location.href='../vista/index.php';</script>");
	?> 
<html lang="es">
		<tbody>
			<?php while($row = mysqli_fetch_assoc($resultado)){ ?>
			<tr>
			<td><?php echo $row['tip_doc']."-".$row['doc_paciente']; ?></td>
			<td><?php echo $row['nombre']." ".$row['apellidos']; ?></td>
			<td><?php echo $row['direccion']; ?></td>
			<td><?php echo $row['telefono']; ?></td>
			<td><?php echo $row['sexo']; ?></td>
			<td><?php echo $row['fecha_naci']; ?></td>
			<td><?php echo $row['lugar_naci']; ?></td>
			<td><?php echo $row['escolaridad']; ?></td>
			<td><?php echo $row['lateralidad']; ?></td>
			<td><a href="index.php?page=actu_aprendiz&id=<?php echo $row['doc_paciente']; ?>"><span class="glyphicon glyphicon-pencil"></span></span></a></td>
		</tr>
	<?php
		}	
	?>
</tbody>
</table>
</div>
</div>
</body>
</html>