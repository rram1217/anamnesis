<div class="container">
	<div id="signupbox" style="margin-top:60px" class="mainbox col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
			<div class="panel panel-info">
				<div class="panel-heading">
				<div class="panel-title"><h3 style="text-align:center"> Registrar Paciente</h3> 
				</div>
				</div>
				<div style="float:right; font-size:50%; position:relative; top:-10px"><a id="singinlink" href="menu.php">...</a></div>
		<form class="form-horizontal" method="POST" action="../controlador/registrar_aprendiz_2.php" enctype="multipart/form-data" autocomplete="off">
			<div class="form-group">
				<label class="col-sm-3 control-label">Tipo Documento</label>
				<div class="col-sm-8">
					<label class="radio-inline">
						<input type="radio" name="tipodoc" value="TI."> TI.
					</label>
					<label class="radio-inline">
						<input type="radio" name="tipodoc" value="CC." checked> CC.
					</label>
					<label class="radio-inline">
						<input type="radio" name="tipodoc" value="CE." checked> CE.
					</label>
					<label class="radio-inline">
						<input type="radio" name="tipodoc" value="PS." checked> PS.
					</label>
					<label class="radio-inline">
						<input type="radio" name="tipodoc" value="RC." checked> RC.
					</label>
            </div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Documento</label>
					<div class="col-sm-8">
				<input  class="form-control" type="number" autofocus="" name="cedula" min="1" max="99999999999" placeholder="Documento" value="" required="">
			</div>
		</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Nombres</label>
				<div class="col-sm-8">
				<input class="form-control" type="text"  name="nombres" placeholder="Nombres Paciente" value="" required="">
			</div>	
		</div>
			<div class="form-group">
					<label class="col-sm-3 control-label">Apellidos</label>
					<div class="col-sm-8">
				<input class="form-control" type="text"  name="apellidos" placeholder="Apellidos Paciente" value="" required="">
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">SEXO</label>
				<div class="col-sm-8">
					<label class="radio-inline">
						<input type="radio" name="sexo" value="Maculino"> Masculino.
					</label>
					<label class="radio-inline">
						<input type="radio" name="sexo" value="Femenino" checked> Femenino.
					</label>
					
					
            </div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Fecha Nacimiento</label>
				<div class="col-sm-8">
				<input class="form-control" type="date"  name="F_nacimiento" placeholder="Direccion Paciente" value="" required="">
			</div>
			</div>
			<div class="form-group">
					<label class="col-sm-3 control-label">Lugar Nacimiento</label>
					<div class="col-sm-8">
				<input class="form-control" type="varchar"  name="L_nacimiento" placeholder="Lugar de Nacimiento"  value="" required="">

				 </div>
			</div>
		    <div class="form-group">
					<label class="col-sm-3 control-label">Edad</label>
					<div class="col-sm-8">
				<input class="form-control" type="number"  name="edad" placeholder="edad" value="" required="">
			</div>
			</div>
			
		    <div class="form-group">
					<label class="col-sm-3 control-label">Escolaridad</label>
					<div class="col-sm-8">
				<input class="form-control" type="varchar"  name="escolaridad" placeholder="Escolaridad" value="" required="">
			</div>
			 </div>
		    <div class="form-group">
					<label class="col-sm-3 control-label">Direccion </label>
					<div class="col-sm-8">
				<input class="form-control" type="varchar"  name="direccion" placeholder="Direccion del Paciente" value="" required="">
			</div>
			</div>
			<div class="form-group">
					<label class="col-sm-3 control-label">Telefono </label>
					<div class="col-sm-8">
				<input class="form-control" type="number"  name="telefono" placeholder="Telefono del Paciente" value="" required="">
			</div>
			</div>
			<div class="form-group">
					<label class="col-sm-3 control-label">Ocupacion </label>
					<div class="col-sm-8">
				<input class="form-control" type="varchar"  name="ocupacion" placeholder="Ocupacion del Paciente" value="" required="">
			</div>
			</div>
			<div class="form-group">
					<label class="col-sm-3 control-label">Eps </label>
					<div class="col-sm-8">
				<input class="form-control" type="varchar"  name="eps" placeholder="Eps del Paciente" value="" required="">
			</div>
			</div>
			<div class="form-group">
					<label class="col-sm-3 control-label">Tarjeta de vinculacion </label>
					<div class="col-sm-8">
				<input class="form-control" type="varchar"  name="t_vinculacion" placeholder="T. Vinculacion del Paciente" value="" required="">
			</div>
			</div>
			<div class="form-group">
					<label class="col-sm-3 control-label">Laterialidad </label>
					<div class="col-sm-8">
				<input class="form-control" type="varchar"  name="laterialidad" placeholder="Laterialidad del Paciente" value="" required="">
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Estado Civil</label>
				<div class="col-sm-8">
					<label class="radio-inline">
						<input type="radio" name="estadocivil" value="Soltero"> Soltero.
					</label>
					<label class="radio-inline">
						<input type="radio" name="estadocivil" value="Casado" checked> Casado.
					</label>
					<label class="radio-inline">
						<input type="radio" name="estadocivil" value="Separado" checked> Separado.
					</label>
					<label class="radio-inline">
						<input type="radio" name="estadocivil" value="Viudo" checked> Viudo.
					</label>
					<label class="radio-inline">
						<input type="radio" name="estadocivil" value="Divorciado" checked> Divorciado
					</label>
					
            </div>
			</div>
						
			<div class="form-group" align="center">
					<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
		</form>
	</div>
</div>
</div>