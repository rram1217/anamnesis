<?php
	
	require '../modelo/fpdf/fpdf.php';
	
	class PDF extends FPDF 
	{
		function Header()
		{
			$fecha=date("Y-m-d");
			$this->SetFillColor(255,164,0);
			$this->Image('../img/logo.JPEG', 15, 10, 40 );
			$this->SetFont('Arial','I',8);
     		$this->SetTextColor(140, 140, 140);
			$this->Cell(120);
			$this->Cell(67,4, utf8_decode('Dra. MARIA ALEXANDRA NOVOA'),0,2,'L');
			$this->Cell(67,4, utf8_decode('Neuropsicóloga Clínica - U. San Buenaventura Btá'),0,2,'L');
			$this->Cell(67,4, utf8_decode('T.P.108949'),0,1,'L');
			$this->SetFont('Arial','B',15);
			$this->Ln(3);
			$this->Cell(195,1,'',0,1,'C',1);
			$this->Ln(7);
		}
		
		function Footer()
		{
			$this->SetY(-15);
			$this->SetFont('Arial','I', 8);
			$this->Cell(0,5, 'Pagina '.$this->PageNo().'/{nb}',0,0,'C' );
		}		
	}
?>