-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-09-2018 a las 10:23:04
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `notas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `id` int(11) NOT NULL,
  `codigoficha` int(11) NOT NULL,
  `actividad` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`id`, `codigoficha`, `actividad`, `estado`) VALUES
(8, 1366055, 'SCRUM desarrollo de un software para uso del instructor, sin ningÃºn tipo de gratificaciÃ³n ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendiz`
--

CREATE TABLE `aprendiz` (
  `documento` bigint(11) NOT NULL,
  `tipo_documento` text COLLATE utf8_spanish_ci NOT NULL,
  `nombres_a` text COLLATE utf8_spanish_ci NOT NULL,
  `apellidos_a` text COLLATE utf8_spanish_ci NOT NULL,
  `direccion_a` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_a` double NOT NULL,
  `correo_a` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `sexo_a` text COLLATE utf8_spanish_ci NOT NULL,
  `nombre_familiar` text COLLATE utf8_spanish_ci NOT NULL,
  `telefono_familiar` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `codigoficha` int(11) NOT NULL,
  `acum_retardo` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `aprendiz`
--

INSERT INTO `aprendiz` (`documento`, `tipo_documento`, `nombres_a`, `apellidos_a`, `direccion_a`, `telefono_a`, `correo_a`, `sexo_a`, `nombre_familiar`, `telefono_familiar`, `codigoficha`, `acum_retardo`) VALUES
(88244968, 'cc', 'JHANNER ALEXANDER', 'UREÃ‘A PEÃ‘ALOZA ', 'XXXX', 0, 'jaurena8@misena.edu.co', 'masculino', 'XXXXX', '0000', 1366055, '00:17:02'),
(1007029945, 'cc', 'ANGIE KAROLAY', 'CARRILLO AREVALO', 'Av 10', 3172865231, 'akcarrillo5@misena.edu.co', 'femenino', 'ROCIO AREVALO', '3183865229', 1366055, '00:00:00'),
(1007197251, 'cc', ' JHON EDWIN', 'VILLAMIZAR RAMIREZ', 'xxxxx', 0, 'jevillamizar15@misena.edu.co', 'femenino', 'xxxxxx', '00000', 1366055, '00:18:07'),
(1010100372, 'cc', 'YOVANA CAROLINA', 'LLANES OROZCO ', 'XXXXX', 0, 'ycllanes@misena.edu.co', 'femenino', 'XXXXX', '000000', 1366055, '00:10:17'),
(1090424624, 'cc', 'MANUEL AMILCAR', 'MARTINEZ BLANCO ', 'XXXX', 0, 'mamartinez426@misena.edu.co', 'femenino', 'XXXX', '00000', 1366055, '00:00:00'),
(1090487722, 'cc', 'JONATHAN JOSE', 'JIMENEZ CARDONA ', 'xxxxx', 3024414639, 'jjjimenez56@misena.edu.co', 'masculino', 'Luz cardona', '3506386857', 1366055, '00:00:00'),
(1090509887, 'cc', ' NEYRA LISETH', 'LEON MARIN ', 'xxxxx', 0, 'nlleon70@misena.edu.co', 'femenino', 'xxxxx', '00000', 1366055, '00:17:26'),
(1093768716, 'cc', ' JOAQUIN LEONARDO', 'POSADA AYALA', 'XXXX', 0, 'jposada617@misena.edu.co', 'femenino', 'XXXX', '00000', 1366055, '00:18:25'),
(1093773607, 'cc', 'YURANDIR', 'NUÃ‘EZ RODRIGUEZ', 'calle 9n #9-69', 31214844588, 'ynunez706@misena.edu.co', 'masculino', 'carolina rodriguez', '3115914702', 1366055, '00:09:18'),
(1093782976, 'cc', 'RAFAEL RICARDO', 'ALVAREZ MENDIETA', 'av6', 322, 'rr@misena.edu.co', 'masculino', 'carmenza', '3128407006', 1366055, '01:00:12'),
(1098675353, 'cc', 'fabian alberto ', 'guerrero melgarejo', 'calle 2 motilones ', 3124567869, 'yuran_6@hotmail.com', 'masculino', 'iwtfiua', '72344', 1366057, '00:00:00'),
(1148145475, 'cc', 'ARELIYS MARIBEL', 'BASTO GONZALEZ ', 'xxxxxx', 0, 'ambasto5@misena.edu.co', 'femenino', 'xxxxxx', '000000', 1366055, '00:16:46'),
(1193469820, 'cc', 'ANA MARIA', 'VEGA PACHECO ', 'XXXX', 0, 'amvega02@misena.edu.co', 'femenino', 'XXXX', '00000', 1366055, '00:16:20'),
(1193471870, 'cc', 'JADDER ANDRES', 'JAIMES ROJAS ', 'xxxx', 0, 'jajaimes078@misena.edu.co', 'masculino', 'xxxx', '00000', 1366055, '00:14:50'),
(1233488059, 'cc', 'WILMER ANDRES', 'MOJICA CARDONA ', 'XXXX', 0, 'wamojica9@misena.edu.co', 'femenino', 'XXXX', '00000', 1366055, '00:00:00'),
(99031306643, 'cc', 'ARCINIEGAS CHACON', ' KEVIN ENRIQUE', 'xxxxxx', 0, 'kearciniegas3@misena.edu.co', 'femenino', 'xxxxxx', '0000000', 1366055, '00:00:00'),
(99060715110, 'cc', 'KAROL JULIETH', 'BOADA ANGARITA ', 'xxxxxx', 0, 'kjboada0@misena.edu.co', 'femenino', 'xxxxxxx', '000000', 1366055, '00:15:52'),
(99061314691, 'cc', 'DISNEY DAYANNA', 'ANGARITA MENDOZA ', 'xxxxx', 0, 'ddangarita1@misena.edu.co', 'femenino', 'xxxx', '00000', 1366055, '00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desertados`
--

CREATE TABLE `desertados` (
  `documento` bigint(11) NOT NULL,
  `tipo_documento` text COLLATE utf8_spanish_ci NOT NULL,
  `nombres_a` text COLLATE utf8_spanish_ci NOT NULL,
  `apellidos_a` text COLLATE utf8_spanish_ci NOT NULL,
  `direccion_a` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_a` double NOT NULL,
  `correo_a` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `sexo_a` text COLLATE utf8_spanish_ci NOT NULL,
  `nombre_familiar` text COLLATE utf8_spanish_ci NOT NULL,
  `telefono_familiar` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `codigoficha` int(11) NOT NULL,
  `acum_retardo` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `desertados`
--

INSERT INTO `desertados` (`documento`, `tipo_documento`, `nombres_a`, `apellidos_a`, `direccion_a`, `telefono_a`, `correo_a`, `sexo_a`, `nombre_familiar`, `telefono_familiar`, `codigoficha`, `acum_retardo`) VALUES
(1006993256, 'cc', 'JESUS DAVID', 'GUERRERO VILLAMIZAR ', 'xxxxxx', 0, 'jdguerrero65@misena.edu.co', 'masculino', 'xxxxxx', '00000', 1366055, '00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formacion`
--

CREATE TABLE `formacion` (
  `nombreformacion` text COLLATE utf8_spanish_ci NOT NULL,
  `codigoficha` int(11) NOT NULL,
  `fechainiciar` date NOT NULL,
  `fechafin` date NOT NULL,
  `nivel` text COLLATE utf8_spanish_ci NOT NULL,
  `jornada` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(3) NOT NULL,
  `hora_entrada` time NOT NULL,
  `hora_salida` time NOT NULL,
  `documento_u` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `formacion`
--

INSERT INTO `formacion` (`nombreformacion`, `codigoficha`, `fechainiciar`, `fechafin`, `nivel`, `jornada`, `estado`, `hora_entrada`, `hora_salida`, `documento_u`) VALUES
('ANALISIS Y DESARROLLO DE SISTEMAS DE INFORMACIÃ“N', 1366055, '2018-08-12', '2018-10-16', 'tecnologo', 'maÃ±ana', 0, '07:00:00', '13:00:00', 1),
('porcino', 1366057, '2018-08-24', '2018-08-29', 'tecnico', 'maÃ±ana', 1, '07:00:00', '13:00:00', 1),
('gvhjbk', 4567890, '2018-08-29', '2018-09-20', 'tecnologo', 'maÃ±ana', 1, '07:00:00', '20:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblasistenciaentrada`
--

CREATE TABLE `tblasistenciaentrada` (
  `id_e` int(11) NOT NULL,
  `documento` bigint(11) NOT NULL,
  `fecha_e` date NOT NULL,
  `hora_e` time NOT NULL,
  `retardo_e` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tblasistenciaentrada`
--

INSERT INTO `tblasistenciaentrada` (`id_e`, `documento`, `fecha_e`, `hora_e`, `retardo_e`) VALUES
(11, 1007029945, '2018-08-12', '15:59:09', '00:00:00'),
(12, 1093782976, '2018-08-13', '07:01:29', '00:01:29'),
(13, 1010100372, '2018-08-13', '07:10:17', '00:10:17'),
(14, 1093773607, '2018-08-13', '07:10:41', '00:10:41'),
(16, 1193471870, '2018-08-13', '07:14:50', '00:14:50'),
(19, 99060715110, '2018-08-13', '07:15:52', '00:15:52'),
(20, 1193469820, '2018-08-13', '07:16:20', '00:16:20'),
(21, 1148145475, '2018-08-13', '07:16:46', '00:16:46'),
(22, 88244968, '2018-08-13', '07:17:02', '00:17:02'),
(23, 1090509887, '2018-08-13', '07:17:26', '00:17:26'),
(25, 1007197251, '2018-08-13', '07:18:07', '00:18:07'),
(26, 1093768716, '2018-08-13', '07:18:25', '00:18:25'),
(27, 1093782976, '2018-08-16', '19:27:03', '00:27:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblasistenciasalida`
--

CREATE TABLE `tblasistenciasalida` (
  `id_s` int(11) NOT NULL,
  `documento` bigint(11) NOT NULL,
  `fecha_s` date NOT NULL,
  `hora_s` time NOT NULL,
  `retardo_s` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tblasistenciasalida`
--

INSERT INTO `tblasistenciasalida` (`id_s`, `documento`, `fecha_s`, `hora_s`, `retardo_s`) VALUES
(15, 1093773607, '2018-08-13', '12:01:22', '00:58:38'),
(16, 1093782976, '2018-08-16', '19:28:20', '00:31:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblingreso`
--

CREATE TABLE `tblingreso` (
  `id_i` int(11) NOT NULL,
  `documento_u` bigint(11) NOT NULL,
  `fecha_i` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tblingreso`
--

INSERT INTO `tblingreso` (`id_i`, `documento_u`, `fecha_i`) VALUES
(56, 1, '2018-08-13 05:00:49'),
(57, 1, '2018-08-13 05:35:00'),
(58, 88251069, '2018-08-13 06:05:18'),
(59, 1, '2018-08-13 06:36:38'),
(60, 88251069, '2018-08-13 07:14:35'),
(61, 1, '2018-08-13 07:19:04'),
(62, 1, '2018-08-13 07:26:56'),
(63, 1, '2018-08-13 08:13:47'),
(64, 88251069, '2018-08-13 10:20:10'),
(65, 1, '2018-08-13 11:54:14'),
(66, 1, '2018-08-13 11:55:22'),
(67, 1, '2018-08-13 12:02:10'),
(68, 1, '2018-08-13 01:19:48'),
(69, 1, '2018-08-14 16:10:16'),
(70, 1, '2018-08-16 10:19:46'),
(71, 1, '2018-08-17 15:37:40'),
(72, 1, '2018-08-17 13:30:43'),
(73, 1, '2018-08-21 12:04:00'),
(74, 1, '2018-08-24 16:31:50'),
(75, 1, '2018-08-29 07:17:07'),
(76, 1217, '2018-09-01 10:03:08'),
(77, 1, '2018-09-01 10:47:03'),
(78, 1, '2018-09-01 12:23:45'),
(79, 1, '2018-09-02 16:47:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblnotas`
--

CREATE TABLE `tblnotas` (
  `id` int(11) NOT NULL,
  `documento` bigint(11) NOT NULL,
  `id_act` int(11) NOT NULL,
  `nota1` float DEFAULT NULL,
  `nota2` float DEFAULT NULL,
  `nota3` float DEFAULT NULL,
  `nota4` float DEFAULT NULL,
  `nota5` float DEFAULT NULL,
  `nota6` float DEFAULT NULL,
  `nota7` float DEFAULT NULL,
  `nota8` float DEFAULT NULL,
  `nota9` float DEFAULT NULL,
  `nota10` float DEFAULT NULL,
  `definitiva` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tblnotas`
--

INSERT INTO `tblnotas` (`id`, `documento`, `id_act`, `nota1`, `nota2`, `nota3`, `nota4`, `nota5`, `nota6`, `nota7`, `nota8`, `nota9`, `nota10`, `definitiva`) VALUES
(35, 88244968, 8, 0, 1.5, 0, 0, 0, 0, 0, 0, 0, 0, 1.5),
(37, 1007029945, 8, 3.5, 4, 5, 5, 5, 4, 5, NULL, 0, 0, 4.5),
(38, 1007197251, 8, 3, 3.9, 5, 5, 5, 4, 0.001, 0, 0, 0, 3.70014),
(39, 1010100372, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 1090424624, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(41, 1090487722, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(42, 1090509887, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(43, 1093768716, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(44, 1093773607, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 1093782976, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(46, 1148145475, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(47, 1193469820, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(48, 1193471870, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(49, 1233488059, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(50, 99031306643, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(51, 99060715110, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(52, 99061314691, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuarios`
--

CREATE TABLE `tblusuarios` (
  `documento_u` bigint(11) NOT NULL,
  `contrasena_u` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tblusuarios`
--

INSERT INTO `tblusuarios` (`documento_u`, `contrasena_u`) VALUES
(1, 'adsi1366055'),
(1217, 'rram1217'),
(88251069, '88251069');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codigoficha` (`codigoficha`);

--
-- Indices de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD PRIMARY KEY (`documento`),
  ADD KEY `ficha_a` (`codigoficha`);

--
-- Indices de la tabla `desertados`
--
ALTER TABLE `desertados`
  ADD PRIMARY KEY (`documento`),
  ADD KEY `ficha_a` (`codigoficha`);

--
-- Indices de la tabla `formacion`
--
ALTER TABLE `formacion`
  ADD PRIMARY KEY (`codigoficha`),
  ADD KEY `id_usuario` (`documento_u`);

--
-- Indices de la tabla `tblasistenciaentrada`
--
ALTER TABLE `tblasistenciaentrada`
  ADD PRIMARY KEY (`id_e`),
  ADD KEY `documento_estudiante_asistencia` (`documento`);

--
-- Indices de la tabla `tblasistenciasalida`
--
ALTER TABLE `tblasistenciasalida`
  ADD PRIMARY KEY (`id_s`),
  ADD KEY `documento_estudiante_asistencia_salida` (`documento`);

--
-- Indices de la tabla `tblingreso`
--
ALTER TABLE `tblingreso`
  ADD PRIMARY KEY (`id_i`),
  ADD KEY `documento_ingreso` (`documento_u`);

--
-- Indices de la tabla `tblnotas`
--
ALTER TABLE `tblnotas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_a` (`documento`),
  ADD KEY `id_act` (`id_act`);

--
-- Indices de la tabla `tblusuarios`
--
ALTER TABLE `tblusuarios`
  ADD PRIMARY KEY (`documento_u`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tblasistenciaentrada`
--
ALTER TABLE `tblasistenciaentrada`
  MODIFY `id_e` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `tblasistenciasalida`
--
ALTER TABLE `tblasistenciasalida`
  MODIFY `id_s` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `tblingreso`
--
ALTER TABLE `tblingreso`
  MODIFY `id_i` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT de la tabla `tblnotas`
--
ALTER TABLE `tblnotas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `actividad_ibfk_1` FOREIGN KEY (`codigoficha`) REFERENCES `formacion` (`codigoficha`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD CONSTRAINT `aprendiz_ibfk_1` FOREIGN KEY (`codigoficha`) REFERENCES `formacion` (`codigoficha`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `desertados`
--
ALTER TABLE `desertados`
  ADD CONSTRAINT `desertados_ibfk_1` FOREIGN KEY (`codigoficha`) REFERENCES `formacion` (`codigoficha`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `formacion`
--
ALTER TABLE `formacion`
  ADD CONSTRAINT `formacion_ibfk_1` FOREIGN KEY (`documento_u`) REFERENCES `tblusuarios` (`documento_u`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tblasistenciaentrada`
--
ALTER TABLE `tblasistenciaentrada`
  ADD CONSTRAINT `tblasistenciaentrada_ibfk_1` FOREIGN KEY (`documento`) REFERENCES `aprendiz` (`documento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tblasistenciasalida`
--
ALTER TABLE `tblasistenciasalida`
  ADD CONSTRAINT `tblasistenciasalida_ibfk_1` FOREIGN KEY (`documento`) REFERENCES `aprendiz` (`documento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tblingreso`
--
ALTER TABLE `tblingreso`
  ADD CONSTRAINT `tblingreso_ibfk_1` FOREIGN KEY (`documento_u`) REFERENCES `tblusuarios` (`documento_u`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tblnotas`
--
ALTER TABLE `tblnotas`
  ADD CONSTRAINT `tblnotas_ibfk_1` FOREIGN KEY (`documento`) REFERENCES `aprendiz` (`documento`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tblnotas_ibfk_2` FOREIGN KEY (`id_act`) REFERENCES `actividad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
