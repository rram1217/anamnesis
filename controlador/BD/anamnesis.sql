-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-05-2019 a las 06:57:11
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `anamnesis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuantitativo`
--

CREATE TABLE `cuantitativo` (
  `cuantitativo_id` double NOT NULL,
  `historia_id` double NOT NULL,
  `pruebas_r` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `puntaje_e` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `paciente` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `clasificacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cuantitativo`
--

INSERT INTO `cuantitativo` (`cuantitativo_id`, `historia_id`, `pruebas_r`, `puntaje_e`, `paciente`, `clasificacion`) VALUES
(6, 38, ' dc hbhb', 'bhbhhbbhbhb ', 'hbhbhhbnhhyub ', 'hbhbhb '),
(7, 38, 'hb ', 'hb ', 'hb ', 'hb '),
(8, 38, 'hb ', 'hb ', 'h ', 'bh '),
(9, 38, 'bh ', 'b ', 'hb ', 'hb '),
(10, 38, 'hb ', 'hb ', 'hb ', 'h '),
(11, 38, 'bh ', 'b ', 'hb ', 'hb '),
(12, 38, 'hb ', 'hb ', 'hb ', 'h '),
(13, 38, 'bh ', 'bh ', 'b ', 'hb '),
(14, 38, 'hb ', 'hb ', 'h ', 'bh '),
(15, 38, ' ', ' ', ' ', ' '),
(16, 38, ' ', ' ', ' ', ' '),
(17, 38, ' ', ' ', ' ', ' '),
(18, 38, ' ', ' ', ' ', ' '),
(19, 38, ' ', ' ', ' ', ' '),
(20, 38, ' ', ' ', ' ', ' '),
(21, 39, ' b gvbgvb', 'gvbgbg ', 'gbgbgb ', 'gbgbgbgb '),
(22, 39, ' ', ' ', ' ', ' '),
(23, 39, ' ', ' ', ' ', ' '),
(24, 39, ' ', ' ', ' ', ' '),
(25, 39, ' ', ' ', ' ', ' '),
(26, 39, ' ', ' ', ' ', ' '),
(27, 39, ' ', ' ', ' ', ' '),
(28, 39, ' ', ' ', ' ', ' '),
(29, 39, ' ', ' ', ' ', ' '),
(30, 39, ' xhbgh', 'bhbhb ', 'hbhbhb ', 'hbhb '),
(31, 39, 'hbhb ', 'hbhbh ', ' ', ' '),
(32, 39, ' ', ' ', ' ', ' '),
(33, 39, ' ', ' ', ' ', ' '),
(34, 39, ' ', ' hbhb', ' ghbgyb', ' '),
(35, 39, ' ', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historia`
--

CREATE TABLE `historia` (
  `historia_id` double NOT NULL,
  `paciente_id` double NOT NULL,
  `usuario_id` double NOT NULL,
  `fecha_recepcion` date NOT NULL,
  `fecha_cierre` date NOT NULL,
  `remitido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `responsable` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `motivo_c` varchar(2000) COLLATE utf8_spanish_ci NOT NULL,
  `patologico` varchar(2000) COLLATE utf8_spanish_ci NOT NULL,
  `quirurgicos` varchar(2000) COLLATE utf8_spanish_ci NOT NULL,
  `traumaticos` varchar(2000) COLLATE utf8_spanish_ci NOT NULL,
  `toxico_alergico` varchar(2000) COLLATE utf8_spanish_ci NOT NULL,
  `farmacologicos` varchar(2000) COLLATE utf8_spanish_ci NOT NULL,
  `familiares` varchar(2000) COLLATE utf8_spanish_ci NOT NULL,
  `psiquiatricos` varchar(2000) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `historia`
--

INSERT INTO `historia` (`historia_id`, `paciente_id`, `usuario_id`, `fecha_recepcion`, `fecha_cierre`, `remitido`, `responsable`, `motivo_c`, `patologico`, `quirurgicos`, `traumaticos`, `toxico_alergico`, `farmacologicos`, `familiares`, `psiquiatricos`) VALUES
(7, 1, 1, '2019-05-01', '2019-05-07', 'medimas', 'rafael', 'accidente laboral', 'NINGUNO', 'NINGUNO', 'NINGUNO', 'NINGUNO', 'NINGUNO', 'NINGUNO', 'NINGUNO'),
(8, 1, 1, '2019-05-01', '2019-05-07', 'medimas', 'rafael', 'accidente laboral', 'NINGUNO', 'NINGUNO', 'NINGUNO', 'NINGUNO', 'NINGUNO', 'NINGUNO', 'NINGUNO'),
(9, 1, 1, '2019-05-01', '2019-05-06', 'salud', 'ninh', 'hjbshjb', 'hbhsx', 'hbhb', 'hbhb', 'hbhb', 'hbhb', 'hbh', 'hb'),
(10, 1, 1, '2019-05-01', '2019-05-07', 'salud', 'wilmer', 'njsxbhb', 'hbnshb', 'hbhb', 'hbhb', 'hbhb', 'hbhb', 'hbh', 'hbbbb'),
(11, 1, 1, '2019-05-01', '2019-05-07', 'salud', 'wilmer', 'njsxbhb', 'hbnshb', 'hbhb', 'hbhb', 'hbhb', 'hbhb', 'hbh', 'hbbbb'),
(12, 1, 1, '2019-05-01', '2019-05-07', 'salud', 'wilmer', 'njsxbhb', 'hbnshb', 'hbhb', 'hbhb', 'hbhb', 'hbhb', 'hbh', 'hbbbb'),
(13, 1, 1, '2019-05-01', '2019-05-05', 'salud', 'rafael', 'jhbhxsab', 'hbhb', 'hbhb', 'hbhb', 'hbhb', 'hbh', 'hbh', 'hbh'),
(14, 1, 1, '2019-05-07', '2019-05-07', 'ahb', 'hhbh', 'hbhbhb', 'hbhb', 'hbhb', 'hbhb', 'hbhb', 'hbhb', 'hbhb', 'hbh'),
(15, 1, 1, '2019-04-30', '2019-05-07', 'hbshb', 'hbhb', 'hbhbhb', 'hbhb', 'hbh', 'b', 'hbhb', 'hbhbh', 'hbhb', 'hbhb'),
(16, 1, 1, '2019-04-29', '2019-05-07', 'njbhsb', 'hbhbhb', 'hbhbhb', 'hbhbhb', 'hbhbhb', 'hbhbhb', 'hbhbhy', 'hbhbhb', 'hbhbhb', 'hbhbhbh'),
(17, 1, 1, '2019-04-29', '2019-05-07', 'njbhsb', 'hbhbhb', 'hbhbhb', 'hbhbhb', 'hbhbhb', 'hbhbhb', 'hbhbhy', 'hbhbhb', 'hbhbhb', 'hbhbhbh'),
(18, 1, 1, '2019-04-28', '2019-05-07', 'bvg', 'hgghbv', 'HGVGHV', 'GVGVGV', 'GVGV', 'GV', 'GVGV', 'GV', 'GV', 'GVGV'),
(19, 1, 1, '2019-04-28', '2019-05-07', 'bvg', 'hgghbv', 'HGVGHV', 'GVGVGV', 'GVGV', 'GV', 'GVGV', 'GV', 'GV', 'GVGV'),
(20, 1, 1, '2019-04-28', '2019-04-29', 'JHBHUYB', 'HBHBH', 'HHBHB', 'HBHBH', 'HBHB', 'HBHB', 'HBHBH', 'HBHB', 'HBHB', 'HBHB'),
(21, 1, 1, '2019-04-29', '2019-04-28', 'HGVGHV', 'HVHV', 'HVHVH', 'HVHVH', 'VHV', 'HVHVHV', 'HVHVHG', 'HVHVH', 'HVHVH', 'VHVH'),
(22, 1, 1, '2019-04-29', '2019-04-28', 'HGVGHV', 'HVHV', 'HVHVH', 'HVHVH', 'VHV', 'HVHVHV', 'HVHVHG', 'HVHVH', 'HVHVH', 'VHVH'),
(23, 1, 1, '2019-04-29', '2019-04-28', 'HGVGHV', 'HVHV', 'HVHVH', 'HVHVH', 'VHV', 'HVHVHV', 'HVHVHG', 'HVHVH', 'HVHVH', 'VHVH'),
(24, 1, 1, '2019-04-28', '2019-05-07', 'ggvgvv', 'gvgvgv', 'gvgv', 'gv', 'gvgv', 'gvgv', 'gvg', 'vgv', 'gv', 'gvg'),
(25, 1, 1, '2019-04-28', '2019-05-07', 'ggvgvv', 'gvgvgv', 'gvgv', 'gv', 'gvgv', 'gvgv', 'gvg', 'vgv', 'gv', 'gvg'),
(26, 1, 1, '2019-04-29', '2019-05-07', 'hbhbh', 'hbhbhb', 'hbhbhbhb', 'hbhbhb', 'hb', 'hb', 'hb', 'hb', 'hb', 'hb'),
(27, 1, 1, '2019-04-29', '2019-05-07', 'hbhbh', 'hbhbhb', 'hbhbhbhb', 'hbhbhb', 'hb', 'hb', 'hb', 'hb', 'hb', 'hb'),
(28, 1, 1, '2019-04-29', '2019-05-07', 'hbhbh', 'hbhbhb', 'hbhbhbhb', 'hbhbhb', 'hb', 'hb', 'hb', 'hb', 'hb', 'hb'),
(29, 1, 1, '2019-04-29', '2019-05-07', 'hbhbh', 'hbhbhb', 'hbhbhbhb', 'hbhbhb', 'hb', 'hb', 'hb', 'hb', 'hb', 'hb'),
(30, 1, 1, '2019-04-29', '2019-05-07', 'hbhbh', 'hbhbhb', 'hbhbhbhb', 'hbhbhb', 'hb', 'hb', 'hb', 'hb', 'hb', 'hb'),
(31, 1, 1, '2019-04-29', '2019-05-07', 'hbhbh', 'hbhbhb', 'hbhbhbhb', 'hbhbhb', 'hb', 'hb', 'hb', 'hb', 'hb', 'hb'),
(32, 1, 1, '2019-04-29', '2019-05-07', 'hbhbh', 'hbhbhb', 'hbhbhbhb', 'hbhbhb', 'hb', 'hb', 'hb', 'hb', 'hb', 'hb'),
(33, 1, 1, '2019-04-29', '2019-05-07', 'hbhbh', 'hbhbhb', 'hbhbhbhb', 'hbhbhb', 'hb', 'hb', 'hb', 'hb', 'hb', 'hb'),
(34, 1, 1, '2019-05-01', '2019-05-01', 'hgvvg', 'hghbghg', 'hjhghgh', 'hjghjghg', 'hghghg', 'hghghg', 'hghghg', 'hghghg', 'hghghg', 'hghg'),
(35, 1, 1, '2019-05-08', '2019-05-02', 'hjghb', 'hjbhbh', 'hbhb', 'hhbhb', 'hbhbhb', 'hb', 'hbhb', 'hb', 'hbhb', 'hbhb'),
(36, 1, 1, '0000-00-00', '0000-00-00', 'dhdg', 'fghfgh', 'xdfgdfhd', '', '', '', '', '', '', ''),
(37, 1, 1, '0000-00-00', '0000-00-00', 'asdbn', 'sdfg', 'asdfghj', '', '', '', '', '', '', ''),
(38, 1, 1, '2019-05-07', '2019-05-07', 'medimas', 'den', 'ndjhnbwehdb', '', '', '', '', '', '', ''),
(39, 2, 1, '2019-04-29', '2019-05-07', 'salud vida', 'hbhb', 'hbhbhbhb', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memoria`
--

CREATE TABLE `memoria` (
  `memoria_id` double NOT NULL,
  `historia_id` double NOT NULL,
  `escala_trastorno` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `puntaje_esperado` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `orientacion` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `r_orientacion` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `control_mental` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `r_control` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `memoria_logica` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `r_logica` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `memoria_digitos` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `r_digitos` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `aprendizaje_asociado` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `r_aprendizaje` varchar(10) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `memoria`
--

INSERT INTO `memoria` (`memoria_id`, `historia_id`, `escala_trastorno`, `puntaje_esperado`, `orientacion`, `r_orientacion`, `control_mental`, `r_control`, `memoria_logica`, `r_logica`, `memoria_digitos`, `r_digitos`, `aprendizaje_asociado`, `r_aprendizaje`) VALUES
(1, 39, ' 100', 'ninguno ', '667 ', '777 ', '677 ', '677 ', '6678 ', '888 ', '888 ', '888 ', '9996 ', '666 ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `paciente_id` double NOT NULL,
  `doc_paciente` double NOT NULL,
  `tip_doc` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `sexo` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_naci` date NOT NULL,
  `lugar_naci` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `edad` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `escolaridad` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `ocupacion` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `eps` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `tarj_vinculacion` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `lateralidad` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `estado_civil` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`paciente_id`, `doc_paciente`, `tip_doc`, `nombre`, `apellidos`, `sexo`, `fecha_naci`, `lugar_naci`, `edad`, `escolaridad`, `direccion`, `telefono`, `ocupacion`, `eps`, `tarj_vinculacion`, `lateralidad`, `estado_civil`) VALUES
(1, 1233488059, 'CC.', 'WILMER ANDRES', 'MOJICA CARDONA', 'Maculino', '1997-05-03', 'CUCUTA NORTE DE SANTANDER', '22', 'TECNOLOGO', 'CALLE 27 # 1-70', '3057626330', 'DESEMPELADO', 'MEDIMAS', '878092', 'NINGUNA', 'Soltero'),
(2, 88210222, 'CC.', 'NELSON', 'MOJICA SANTANDER', 'Maculino', '1974-06-01', 'DURANIA NORTE DE SANTANDER', '45', 'TECNICO', 'CALLE 27 # 1-70 VIRGILIO BARCO', '3112975575', 'INDEPENDIENTE', 'SALUD VIDA', '76578', 'NINGUNA', 'Separado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` double NOT NULL,
  `doc_usuario` double NOT NULL,
  `tipo_doc` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `tarj_profesional` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `doc_usuario`, `tipo_doc`, `nombre`, `apellidos`, `email`, `tarj_profesional`, `usuario`, `contrasena`) VALUES
(1, 1233488059, 'cedula ciudadania', 'wilmer andres', 'mojica cardona', 'wmojicacardona@gmail.com', '1.233.488059_5', 'WMOJICA', 'Compuwamc'),
(2, 1233488059, 'cedula ciudadania', 'wilmer andres', 'mojica cardona', 'wmojicacardona@gmail.com', '1.233.488059_5', 'WMOJICA', 'Compuwamc');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuantitativo`
--
ALTER TABLE `cuantitativo`
  ADD PRIMARY KEY (`cuantitativo_id`),
  ADD KEY `historia_id` (`historia_id`);

--
-- Indices de la tabla `historia`
--
ALTER TABLE `historia`
  ADD PRIMARY KEY (`historia_id`),
  ADD KEY `paciente_id` (`paciente_id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `memoria`
--
ALTER TABLE `memoria`
  ADD PRIMARY KEY (`memoria_id`),
  ADD KEY `historia_id` (`historia_id`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`paciente_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuantitativo`
--
ALTER TABLE `cuantitativo`
  MODIFY `cuantitativo_id` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `historia`
--
ALTER TABLE `historia`
  MODIFY `historia_id` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `memoria`
--
ALTER TABLE `memoria`
  MODIFY `memoria_id` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
  MODIFY `paciente_id` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuantitativo`
--
ALTER TABLE `cuantitativo`
  ADD CONSTRAINT `cuantitativo_ibfk_1` FOREIGN KEY (`historia_id`) REFERENCES `historia` (`historia_id`);

--
-- Filtros para la tabla `historia`
--
ALTER TABLE `historia`
  ADD CONSTRAINT `historia_ibfk_1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`paciente_id`),
  ADD CONSTRAINT `historia_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`);

--
-- Filtros para la tabla `memoria`
--
ALTER TABLE `memoria`
  ADD CONSTRAINT `memoria_ibfk_1` FOREIGN KEY (`historia_id`) REFERENCES `historia` (`historia_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
